let nome = "Marcus";
let idade = 21;
let isProfessor = false;

console.log("Tipo da variável nome: " + typeof(nome));
console.log("Tipo da variável idade: " + typeof(idade));
console.log(`Tipo da variável isProfessor: ${typeof(isProfessor)}`);